package ru.tsc.denisturovsky.tm.api.repository.dto;

import ru.tsc.denisturovsky.tm.dto.model.SessionDTO;

public interface ISessionDTORepository extends IUserOwnedDTORepository<SessionDTO> {

}